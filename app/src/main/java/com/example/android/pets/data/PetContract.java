package com.example.android.pets.data;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;

/**
 * API Contract for the Pets app.
 */

public final class PetContract {

    private PetContract() {}

    /* URI BASE_CONTENT_URI
       *
       * concatenate the CONTENT_AUTHORITY constant with the scheme
       * “content://” we will create the BASE_CONTENT_URI
       * which will be shared by every URI associated with PetContract.
       * */

    public static  final String CONTENT_AUTHORITY = "com.example.android.pets";
    /*use the parse method which takes in a URI string and returns Uri*/
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    /* PATH_TableName
    *
    * This constants stores the path for each of the tables
    * which will be appended to the base content URI.
    * */
    public static final String PATH_PETS = "pets";

    /* Inner class that defines the table contents */
    public static abstract class PetEntry implements BaseColumns {

        /* Complete CONTENT_URI
         *
         * The content URI to access the pet data in the provider
         *
         * The Uri.withAppendedPath() method appends the BASE_CONTENT_URI,
         * (which contains the scheme and the content authority) to the path segment.
         **/
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_PETS);

        /** Name of database table for pets */
        public static final String TABLE_NAME = "pets";
        /**
         * Unique ID number for the pet (only for use in the database table).
         *
         * Type: INTEGER
         */
        public static final String _ID = BaseColumns._ID;

        /**
         * Name of the pet.
         *
         * Type: TEXT
         */
        public static final String COLUMN_PET_NAME = "name";

        /**
         * Breed of the pet.
         *
         * Type: TEXT
         */
        public static final String COLUMN_PET_BREED = "breed";

        /**
         * Gender of the pet.
         *
         * The only possible values are {@link #GENDER_UNKNOWN}, {@link #GENDER_MALE},
         * or {@link #GENDER_FEMALE}.
         *
         * Type: INTEGER
         */
        public static final String COLUMN_PET_GENDER = "gender";

        /**
         * Weight of the pet.
         *
         * Type: INTEGER
         */
        public static final String COLUMN_PET_WEIGHT = "weight";

        /**
         * Possible values for the gender of the pet.
         *INTEGER (one of 3 gender types 0 = unknown, 1 = male, 2 = female
         * */
        public static final int GENDER_UNKNOWN = 0;
        public static final int GENDER_MALE = 1;
        public static final int GENDER_FEMALE = 2;

        public static boolean isGenderValid(int gender) {
            if (
                    gender == GENDER_UNKNOWN ||
                    gender == GENDER_FEMALE ||
                    gender == GENDER_MALE) {
                return true;
            }
            return false;
        }
        /**
         * The MIME type of the {@link #CONTENT_URI} for a list of pets.
         */
        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PETS;

        /**
         * The MIME type of the {@link #CONTENT_URI} for a single pet.
         */
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PETS;

    }
}
